import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { userLogout } from "@/actions/userAction";
import {useRouter} from 'next/router';
import { useSelector } from 'react-redux';


const Logout = () =>{

    const dispatch =  useDispatch()
    const router = useRouter()
    const user = useSelector(state =>state)

    useEffect(() =>{
        localStorage.removeItem("token")
        dispatch(userLogout())
        console.log(user)
        console.log("token")
        router.push("/")

    })

    return null
}

export default Logout
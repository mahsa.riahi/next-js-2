import React from "react"
import Head from "next/head"

const Home = () => {

  return (
    <div>
        <Head>
            <title>
              Bookapo
            </title>
        </Head>
    </div>
  )
}

export default Home
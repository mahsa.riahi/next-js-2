import React , {useState } from "react";
import Head from "next/head"
import {useDispatch} from "react-redux"
import { userLogin } from "@/actions/userAction";
import { useRouter } from 'next/router';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';

const Login = () => {

    const dispatch = useDispatch()
    const router=useRouter()

    const [ email , setEmail ] = useState("")
    const [ pass , setPass ] = useState("")  
    const [ name , setName ] = useState("Admin")  
  

    const submitValue = React.useMemo(
        () => ({ handleSubmit : (event) =>{
                        event.preventDefault()

                        if (email=="admin@gmail.com" && pass== "admin"){
                            const user={email , name}
                            localStorage.setItem("token" , user)
                            dispatch(userLogin(user))
                            router.replace("/dashboard")
                            }
                        else{
                            setPass("")
                            setEmail("")
                                }
                  },
                  }), [email , pass] )


  const handleEmailChange = (e) => {
        setEmail(e.target.value)
  }
  const handlePassChange = (e) => {
       setPass(e.target.value)
  }

    return(
    <Box >
        <Head>
            <title>
                ورود
            </title>
        </Head>
        <Container fixed
                            sx={{m:"auto" ,
                                    py:5 ,
                                    mt:20,
                                    boxShadow: (theme) => theme.bShadow.default,
                                    width: {xs:420 , sm:470}
                                  }}
        >
            <Typography variant="h5"
                                    component="h1"
                                    sx={{ fontWeight:"bolder" ,
                                              textAlign:"center",
                                             fontSize:"28px",
                                             color: (theme) => theme.pallete.text.primary
                                            }}
            >
                ورود به حساب کاربری
            </Typography>
            <Box sx={{mt:4 , 
                              mb:0.8
                            }}
            >                             
                <form onSubmit={submitValue.handleSubmit}>
                    <Box sx={{display:"flex",
                                     alignItems: 'flex-start',
                                     flexDirection: 'column',
                                     flexWrap: 'wrap',
                                     justifyContent: 'center',
                                     alignItems: 'center',
                                     alignContent: 'center',
                                     gap:1.5,
                                     width:91/100,
                                     m:"auto"
                                   }}
                    >
                        <TextField type="email"
                                            variant="outlined"
                                            placeholder="نام کاربری" 
                                            value={email} 
                                            onChange={handleEmailChange}    
                                            fullWidth
                        />
                         <TextField type="password" 
                                            variant="outlined"
                                            placeholder="رمز ورود" 
                                            value={pass}        
                                            onChange={handlePassChange} 
                                            fullWidth
                        />
                    </Box>
                    <Button variant="contained"
                          component="button"
                          type="submit"
                          sx={{width:180 ,
                                  height:50 ,
                                  fontWeight:"bolder" ,
                                  fontSize:"21px" ,
                                  mt:4 ,
                                  mr:(theme) => theme.mr[1],
                                  bgcolor: (theme) => theme.pallete.primary.light,
                                  ":hover":{
                                    bgcolor: (theme) => theme.pallete.primary.dark
                                  },
                                }}
                    >
                        ورود
                    </Button>  
                </form>
            </Box> 
        </Container>
    </Box>
            
    )
}

export default Login
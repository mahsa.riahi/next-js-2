import React ,{Fragment} from "react";
import Head from "next/head"
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

const Product = ({data}) =>{

return(
<Fragment>
    <Head>
        <title>
            {data.titleFa}
        </title>
    </Head>
    <Box sx={{display: 'flex',
                     alignItems: 'flex-start',
                     flexDirection: 'row',
                     flexWrap: 'wrap',
                     justifyContent: 'center',
                     alignItems: 'center',
                     alignContent: 'center',
                     gap:4,
                     width:3/4,
                    m:"auto",
                    my:15,
                    boxShadow: (theme) => theme.bShadow.default,
                    p:4
                     }} 
    >
        <Box component="img"
                  src={data.cover}
                  alt="book-cover"
                  sx={{ width:280,
                            height:400,
                            boxShadow: (theme) => theme.bShadow.default ,
                        }}
        />            
        <Box component="section"
                 sx={{width: 3/4 ,
                        flexGrow:1
                       }}
        >
            <Typography variant="h5"
                                   component="h1"
                                   sx={{fontWeight:"bolder",
                                            color: (theme) => theme.pallete.text.primary
                                         }}
            >
                نام کتاب:
            </Typography>
            <Typography variant="h5"
                                   component="h1"
                                   sx={{fontSize:"20px",
                                            color: (theme) => theme.pallete.text.secondary
                                          }} 
            >   
                {data.titleFa}          
                <br/>
                {data.titleEn}             
            </Typography>
            <Typography variant="h5"
                                    component="h1"
                                    sx={{fontWeight:"bolder" ,
                                            mt:3,
                                            color: (theme) => theme.pallete.text.primary
                                            }}
                >
                    نام نویسنده:
                </Typography>
                <Typography variant="h5"
                                       component="h1"
                                       sx={{fontSize:"20px",
                                               color: (theme) => theme.pallete.text.secondary
                                              }}    
                >
                    {data.author.nameFa === null ? ' ' :  data.author.nameFa}
                    <br/>
                    {data.author.nameEn === null ? ' ' : data.author.nameEn}        
                </Typography>
                <Typography variant="h5"
                                       component="h1"
                                       sx={{fontWeight:"bolder" ,
                                               mt:3,
                                                color: (theme) => theme.pallete.text.primary
                                                }}
                >
                    دسته بندی:
                </Typography>
                    { data.categories.map( item =>     
                        <Typography variant="h5"
                                               key={item.name}
                                                component="h1"
                                                sx={{fontSize:"20px",
                                                        color: (theme) => theme.pallete.text.secondary
                                                       }} 
                        >
                        {item.name}
                        </Typography>
                        )}   

                        {data.seo[1].description === null  
                        ?  ' ' 
                        :  <Box component="div">
                                 <Typography variant="h5"
                                                        component="h1"
                                                        sx={{ fontWeight:"bolder" ,
                                                                  mt:3,
                                                                  color: (theme) => theme.pallete.text.primary
                                                               }}
                                >
                                     توضیحات:
                                </Typography>
                                 <Typography component="p"
                                                        variant="caption" 
                                                        sx={{fontSize:"18px",
                                                                color: (theme) => theme.pallete.text.secondary
                                                              }}>
                                    {data.seo[1].description}
                                </Typography>
                             </Box>
                        }                             
        </Box>   
    </Box> 
</Fragment>

)

}

export async function getStaticPaths() {
    const res = await fetch("https://api.staging.bookapo.com/book/feed/last")
    const data = await res.json()
    const books = data.data.results

    const paths = books.map((book) => ({
        params: { id: book.slug },
      }))

    return {paths , fallback :false}
}

export async function getStaticProps({params}){
    const res = await fetch(`https://api.staging.bookapo.com/book/${params.id}`)
    const data = await res.json()

    return{
        props:{
          data:  data.data
        }
    }
}

export default Product
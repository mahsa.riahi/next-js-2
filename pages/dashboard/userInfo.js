import React from "react"
import { useSelector } from "react-redux"
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography'



const UserInfo = () =>{

const user= useSelector(state=>state)


return(
    <Box
        component="section"
    >
        <Paper variant="outlined" 
                    square
                    sx={{boxShadow: (theme) => theme.bShadow.default  ,
                             p: 4,
                            m:"auto",
                            mt:20 ,
                            width:350}}
        >
            <Typography variant="h5" 
                                   component="h1"
                                   sx={{fontWeight: "bolder",
                                            fontSize:"27px",
                                            color: (theme) => theme.pallete.text.primary
                                          }}
            >
                اطلاعات کاربر:
            </Typography>
            <Box component="div"
                     sx={{ mr:21 ,
                              mt:2
                           }}
            >
                <Typography variant="h6" 
                                       component="h2"
                                       sx={{fontWeight:"bolder",
                                                color: (theme) => theme.pallete.text.primary
                                            }}
                >
                    نام: 
                </Typography>
                <Typography variant="h6" 
                                       component="h2"
                                       sx={{color: (theme) => theme.pallete.text.secondary}}
                >
                    {user.name}
                </Typography>
                <Typography variant="h6" 
                                      component="h2"
                                      sx={{mt:2 ,
                                              fontWeight:"bolder",
                                              color: (theme) => theme.pallete.text.primary
                                           }}
                >
                   نام کاربری:
                </Typography>
                <Typography variant="h6" 
                                       component="h2"
                                       sx={{color: (theme) => theme.pallete.text.secondary}}
                >
                     {user.email}
                </Typography>
            </Box>
        </Paper>
    </Box>
    
)
}

export default UserInfo
import React from "react"
import { useSelector } from "react-redux"
import {isEmpty} from "lodash"
import UserInfo from "./userInfo";
import Invalidation from "./invalidation"

const Dashboard = () =>{

    const user = useSelector ( state => state )

    return(
        <div>
             {isEmpty(user) ? <Invalidation /> : <UserInfo /> }
        </div>
    )
}

export default Dashboard
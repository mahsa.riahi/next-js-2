import React , {useEffect , Fragment} from 'react';
import Header from 'common/header';
import { useDispatch } from "react-redux";
import { userLogin, userLogout } from "../redux/actions/userAction";


const MainLayout = (props) =>{

    const dispatch = useDispatch()
   
    
    useEffect(() =>{
        const token = localStorage.getItem("token")
        if(token){
            const email = "admin@gmail.com"
            const name = "Admin"
            const user = {email , name}
            dispatch(userLogin(user))
        }
        else{
            dispatch(userLogout())

        }
    })
    
    return (
        <Fragment>
            <Header />
                {props.children}
        </Fragment>
    )

}

export default MainLayout

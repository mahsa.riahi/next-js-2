
const initialState = {
    name : "" ,
    email: ""
}
export const userReducer = (state=initialState , action) =>{

    switch (action.type){
        case "USER-LOGIN" :
            return {...action.payload};
        case "USER-LOGOUT":
            return {...action.payload}
        default :
            return state;
    }
}